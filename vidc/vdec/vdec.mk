# ---------------------------------------------------------------------------------
#				MM-CORE-OSS-OMXCORE
# ---------------------------------------------------------------------------------

# Source Path
VDEC_SRC := $(SRCDIR)/vidc/vdec

# cross-compiler flags
CFLAGS := -Wall 
CFLAGS += -Wundef 
CFLAGS += -Wstrict-prototypes 
CFLAGS += -Wno-trigraphs 

# cross-compile flags specific to shared objects
CFLAGS_SO := $(QCT_CFLAGS_SO)

# Preproc flags
CPPFLAGS := $(QCT_CPPFLAGS)

# linker flags for shared objects
LDFLAGS_SO += -shared

# linker flags
LDFLAGS := -L$(SYSROOTLIB_DIR)

# hard coding target for 7630
TARGET := 7630

# definitions
LIBMAJOR := $(basename $(basename $(LIBVER)))
REGISTRY_TABLE_LIBS ?= \
	libmm-vdec-omxh264.so.1 \
	libmm-vdec-omxmp4.so.1 \
	libmm-vdec-omxwmv.so.1 \
	libmm-vdec-omxdivx.so.1

# ---------------------------------------------------------------------------------
#					BUILD
# ---------------------------------------------------------------------------------

all: libOmxVdec.so.$(LIBVER) mm-vdec-omx-test mm-video-driver-test

install:
	if [ ! -d $(SYSROOTLIB_DIR) ]; then mkdir -p $(SYSROOTLIB_DIR); fi
	if [ ! -d $(SYSROOTBIN_DIR) ]; then mkdir -p $(SYSROOTBIN_DIR); fi
	install -m 555 libOmxVdec.so.$(LIBVER) $(SYSROOTLIB_DIR)
	cd $(SYSROOTLIB_DIR) && ln -s libOmxVdec.so.$(LIBVER) libOmxVdec.so.$(LIBMAJOR)
	cd $(SYSROOTLIB_DIR) && ln -s libOmxVdec.so.$(LIBMAJOR) libOmxVdec.so
	for lib in $(REGISTRY_TABLE_LIBS); do                                  \
		cd $(SYSROOTLIB_DIR) && ln -s libOmxVdec.so.$(LIBMAJOR) $$lib; \
	done
	install -m 555 mm-vdec-omx-test $(SYSROOTBIN_DIR)
	install -m 555 mm-video-driver-test $(SYSROOTBIN_DIR)

clean:
	rm -f libOmxVdec.so.$(LIBVER) mm-vdec-omx-test mm-video-driver-test
# ---------------------------------------------------------------------------------
#				COMPILE LIBRARY
# ---------------------------------------------------------------------------------

SRCS := $(VDEC_SRC)/src/frameparser.cpp
SRCS += $(VDEC_SRC)/src/h264_utils.cpp
SRCS += $(VDEC_SRC)/src/omx_vdec.cpp

CPPFLAGS += -I$(VDEC_SRC)/inc
CPPFLAGS += -I$(SYSROOTINC_DIR)/mm-core
CPPFLAGS += -I$(KERNEL_DIR)/include
CPPFLAGS += -I$(KERNEL_DIR)/arch/arm/include

CPPFLAGS += -UENABLE_DEBUG_LOW
CPPFLAGS += -DENABLE_DEBUG_HIGH
CPPFLAGS += -DENABLE_DEBUG_ERROR
CPPFLAGS += -UINPUT_BUFFER_LOG
CPPFLAGS += -UOUTPUT_BUFFER_LOG
CPPFLAGS += -DMAX_RES_720P

LDLIBS := -lpthread
LDLIBS += -lstdc++

libOmxVdec.so.$(LIBVER): $(SRCS)
	$(CC) $(CPPFLAGS) $(CFLAGS) $(CFLAGS_SO) $(LDFLAGS_SO) -Wl,-soname,libOmxVdec.so.$(LIBMAJOR) -o $@ $^ $(LDLIBS)

# ---------------------------------------------------------------------------------
#				COMPILE TEST APP
# ---------------------------------------------------------------------------------

TEST_LDLIBS := -lpthread
TEST_LDLIBS += -lstdc++
TEST_LDLIBS += -lOmxCore

SRCS := $(VDEC_SRC)/src/queue.c
SRCS += $(VDEC_SRC)/test/omx_vdec_test.cpp

mm-vdec-omx-test: $(SRCS)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) -o $@ $^ $(TEST_LDLIBS)

# ---------------------------------------------------------------------------------
#				COMPILE TEST APP
# ---------------------------------------------------------------------------------

SRCS := $(VDEC_SRC)/src/message_queue.c
SRCS += $(VDEC_SRC)/test/decoder_driver_test.c

mm-video-driver-test: $(SRCS)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) -o $@ $^ $(TEST_LDLIBS)

# ---------------------------------------------------------------------------------
#					END
# ---------------------------------------------------------------------------------
