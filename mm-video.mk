all:
	@echo "invoking vidc/vdec make"
	$(MAKE) -f $(SRCDIR)/vidc/vdec/vdec.mk
	@echo "invoking vidc/enc make"
	$(MAKE) -f $(SRCDIR)/vidc/venc/venc.mk

install: all
	@echo "invoking vidc/vdec make install"
	$(MAKE) -f $(SRCDIR)/vidc/vdec/vdec.mk install
	@echo "invoking vidc/enc make install"
	$(MAKE) -f $(SRCDIR)/vidc/venc/venc.mk install

clean:
	$(MAKE) -f $(SRCDIR)/vidc/vdec/vdec.mk clean
	$(MAKE) -f $(SRCDIR)/vidc/venc/venc.mk clean
